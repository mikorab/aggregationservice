package com.cgi.aggregationservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AggregationservicesApplication {

    public static void main(String[] args) {

        SpringApplication.run(AggregationservicesApplication.class, args);
    }

}
